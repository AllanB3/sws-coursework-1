import sys
import csv
import rdflib

uriList = []
field = "Commune"

with open("OCHA Haiti 3W - Data.csv") as csvFile:
    reader = csv.DictReader(csvFile)
    for row in reader:
        townName = row[field]
        townSuffix = "".join(t + "%20" for t in townName.split())[:-3]

        g = rdflib.Graph()
        uri = "http://api.geonames.org/search?name=" + townSuffix + "&type=rdf&username=allanb3"

        g.parse(uri, format="xml")

        currentSubject = ""
        inHaiti = False
        isTown = False
        foundURI = False
        for subject, predicate, value in sorted(g):
            if subject != currentSubject:
                currentSubject = subject
                inHaiti = False
                isTown = False

            if predicate == rdflib.URIRef("http://www.geonames.org/ontology#countryCode") and value == rdflib.Literal("HT"):
                inHaiti = True

            if predicate == rdflib.URIRef("http://www.geonames.org/ontology#featureClass") and value == rdflib.URIRef("http://www.geonames.org/ontology#P"):
                isTown = True

            if inHaiti and isTown:
                uriList.append((townName, currentSubject))
                foundURI = True
                break

        if not foundURI:
            uriList.append((townName, None))

for uri in list(set(uriList)):
    print(uri)
