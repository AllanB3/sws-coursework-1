import csv
import rdflib
from rdflib.namespace import RDF

graph = rdflib.Graph()

townData = open("townURIs.txt", "r").read().splitlines()
townDict = {}

for t in townData:
	townTuple = eval(t)
	townDict[townTuple[0]] = townTuple[1]

with open("OCHA Haiti 3W - Data.csv") as csvFile:
	reader = csv.DictReader(csvFile)

	haitiMission = rdflib.URIRef("http://vocab.inf.ed.ac.uk/sws/s1224425#HaitiMission")
	organization = rdflib.URIRef("http://www.w3.org/ns/org#Organization")
	organizationType = rdflib.URIRef("http://www.agls.gov.au/agls/terms/serviceType")
	activitiesSector = rdflib.URIRef("http://vocab.inf.ed.ac.uk/sws/s1224425#productOrService")
	activitiesType = rdflib.URIRef("http://reegle.info/schema#sector")
	department = rdflib.URIRef("http://www.w3.org/2006/vcard/ns#region")
	pcodeDepartment = rdflib.URIRef("http://vocab.inf.ed.ac.uk/sws/s1224425#departmentOfHaitiPostcode")
	commune = rdflib.URIRef("http://vocab.inf.ed.ac.uk/sws/s1224425#commune")
	pcodeCommune = rdflib.URIRef("https://www.w3.org/2006/vcard/ns#postal-code")

	for i, row in enumerate(reader):
		uri = rdflib.URIRef("http://vocab.inf.ed.ac.uk/sws/s1224425/HaitiMissions#{0}".format(i))
		graph.add((uri, RDF.type, haitiMission))
		for key, value in row.iteritems():
			if key == "Organization":
				graph.add((uri, organization, rdflib.Literal(value)))
			elif key == "Organization type":
				graph.add((uri, organizationType, rdflib.Literal(value)))
			elif key == "Activities sector":
				graph.add((uri, activitiesSector, rdflib.Literal(value)))
			elif key == "Activities type":
				graph.add((uri, activitiesType, rdflib.Literal(value)))
			elif key == "Department":
				graph.add((uri, department, rdflib.Literal(value)))
			elif key == "Pcode Department":
				graph.add((uri, pcodeDepartment, rdflib.Literal(value)))
			elif key == "Commune":
				try:
					graph.add((uri, commune, rdflib.URIRef(townDict[value])))
				except TypeError:
					graph.add((uri, commune, rdflib.Literal(value)))
			elif key == "Pcode Commune":
				graph.add((uri, pcodeCommune, rdflib.Literal(value)))

	graph.serialize("OCHA Haiti 3W - Data.ttl", format="turtle")
